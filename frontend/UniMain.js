import "./style.css";
import { createPublicClient, http, getContract, formatUnits, createWalletClient, custom, parseUnits } from "viem";

import { goerli } from "viem/chains";
import { UNI } from "./abi/UNI";


const publicClient = createPublicClient({
  chain: goerli,
  transport: http(),
});
const [account] = await window.ethereum.request({ method: "eth_requestAccounts" });
const walletClient = createWalletClient({
  account,
  chain: goerli,
  transport: custom(window.ethereum),
});

const uniContract = getContract({
  address: "0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984",
  abi: UNI,
  publicClient,
  walletClient, // additional argument here
});



//await uniContract.write.transfer(["0x20E549fE047FACeFb38959162F037633b96c4376", 1n]);


const blockNumber = await publicClient.getBlockNumber();

/*console.log(await uniContract.read.name());
console.log(decimals);
console.log(formatUnits(await uniContract.read.totalSupply(), decimals));
console.log(formatUnits(await uniContract.read.balanceOf(["0xc2ba723430706885144a37b2EC74feb82A0C1333"]), decimals));
*/

const decimals = await uniContract.read.decimals();

document.querySelector("#app").innerHTML = `
  <div>
    <p>Current block is <span id="blockNumber">${blockNumber}</span></p>

    <h1> ${await uniContract.read.symbol()}</h1>
    <p> Name : ${await uniContract.read.name()}</p>
    <a href="https://goerli.etherscan.io/token/${await uniContract.address}"> Address : ${await uniContract.address}</a>

<p> Total supply : ${formatUnits(await uniContract.read.totalSupply(), decimals)}</p>

<p> Balance of 0x20E549fE047FACeFb38959162F037633b96c4376 : ${formatUnits(await uniContract.read.balanceOf(["0x20E549fE047FACeFb38959162F037633b96c4376"]), decimals)} </p>

  </div>

  <div> 
  <div id="amount">
      <p>Amount : <input id="amountInput"></input></p>
      <button id="maxButton">Max</button>
  </div>
  <div id="recipient">
      <p>Recipient : <input id="recipientInput"></input></p>
      <button id="sendButton">Send</button>
  </div>
  <span id="transaction"></span>



  </div>
`;


document.querySelector("#maxButton").addEventListener("click", async () => {
  document.querySelector("#amountInput").value = formatUnits(await uniContract.read.balanceOf(["0x20E549fE047FACeFb38959162F037633b96c4376"]), decimals);
});

document.querySelector("#sendButton").addEventListener("click", async () => {
  const amount = parseUnits(document.querySelector("#amountInput").value, decimals);
  const recipient = document.querySelector("#recipientInput").value;
  const hash = await uniContract.write.transfer([recipient, amount]);
  document.querySelector("#transaction").innerHTML = `Waiting for tx <a href="https://goerli.etherscan.io/tx/${hash}">${hash}</a>`;
  const transaction = await publicClient.waitForTransactionReceipt({ hash: `${hash}` });
    if (transaction.status == "success") {
        document.querySelector("#transaction").innerHTML = `Transaction <a href="https://goerli.etherscan.io/tx/${hash}">${hash}</a> confirmed!`;
    } else {
        document.querySelector("#transaction").innerHTML = `Transaction <a href="https://goerli.etherscan.io/tx/${hash}">${hash}</a> failed!`;
    };
});
const unwatch = publicClient.watchBlockNumber( 
  { onBlockNumber: blockNumber => document.querySelector("#blockNumber").innerHTML=blockNumber}
)
