import "./style.css";
import { createPublicClient, http, getContract, formatUnits, createWalletClient, custom, parseEther } from "viem";

import { goerli } from "viem/chains";
import { CryptoZombie } from "./abi/cryptoZombie";


const publicClient = createPublicClient({
  chain: goerli,
  transport: http(),
});
const [account] = await window.ethereum.request({ method: "eth_requestAccounts" });
const walletClient = createWalletClient({
  account,
  chain: goerli,
  transport: custom(window.ethereum),
});

const zombieContract = getContract({
  address: "0x0Db13381cb34B5976D9a17188e1bF5153495De84",
  abi: CryptoZombie,
  publicClient,
  walletClient, // additional argument here
});


const blockNumber = await publicClient.getBlockNumber();

const zombieData = await zombieContract.read.zombies([await zombieContract.read.getZombiesByOwner([account])]);

const zombieCount = await zombieContract.read.balanceOf([account]);

const getAllZombies = async () => {
  let count = 0;
  let allZombies = [];

  while (true) {
    try {
      const zombie = await zombieContract.read.zombies([count]);
      allZombies.push(zombie);
      count++;
    } catch (error) {
      break;
    }
  }
  return allZombies;
};

const updateZombiesList = async () => {
  const allZombiesData = await getAllZombies();
  allZombiesList.innerHTML = allZombiesData
    .map((zombie) => `<li class="allZombieCard">${zombie.map((data) => `<p>${data}</p>`).join('')}</li>`)
    .join('');
};

updateZombiesList();

document.querySelector("#app").innerHTML = `
  <div>
    <span id="blockNumber">Current block is ${blockNumber}</span>
    <p>Contract owner : ${await zombieContract.read.owner()}</p>
    <div id="creation">
      <p>Zombie name : <input id="zombieName"></input></p>
      <button id="createZombie">Create</button>
    </div>

    <p id="balance">Zombie count of <b>${await [account]}</b> : ${await zombieContract.read.balanceOf([account])}</p>

    <div id="allZombies">
      <p>My zombie :</p>
      <ul>
        ${await zombieData.map(data => `<li>${data}</li>`).join('\n')}
      </ul>
    </div>
  
    <button id="levelUp">Level up</button>
    <p>All zombies :</p>
    <div id="allZombies">
      <ul id="allZombiesList"></ul>
    </div>
  </div>
`;

document.querySelector("#createZombie").addEventListener("click", async () => {
  const name = document.querySelector("#zombieName").value;
 if (zombieCount > 0) {
  alert("You can just havee 1 zombie");
} else {
  const newZombie = await zombieContract.write.createRandomZombie([name]);
}


});


document.querySelector("#levelUp").addEventListener("click", async () => {
  if (zombieCount === 0) {
    alert("Create zombie before");
    return;
  }

  try {
    
    const levelUpTx = await walletClient.writeContract({
      address: zombieContract.address,
      abi: CryptoZombie,
      functionName: 'levelUp', 
      args: [await zombieContract.read.getZombiesByOwner([account])], 
      value: parseEther('0.001') 
    });

  } catch (error) {
    console.error("Error while the level up", error);
    alert("Error while the level up");
  }
});
